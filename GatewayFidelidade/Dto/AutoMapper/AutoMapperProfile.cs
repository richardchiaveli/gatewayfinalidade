﻿using AutoMapper;
using GatewayFidelidade.Dto.Fidelidade;
using GatewayFidelidade.Dto.Loja;
using Modelo.Fidelidade.Data;
using System;

namespace GatewayFidelidade.Dto.AutoMapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ConsultaFidelizacaoResponseDto, HistoricoConsultaFidelidade>()
                .ForMember(o => o.QuantidadePontos, o => o.MapFrom(m => int.Parse(m.SaldoPontos)))
                .ForMember(o => o.PontosEmReais, o => o.MapFrom(m => !string.IsNullOrEmpty(m.SaldoEmReais) ?
                    decimal.Parse(m.SaldoEmReais) : 0))
                .ForMember(o => o.NsuCliente, o => o.MapFrom((m, a) =>
                {
                    Guid.TryParse(m.NsuCliente, out var nsu);
                    return nsu == Guid.Empty ? Guid.NewGuid() : nsu;
                }))
                .ForMember(o => o.Id, o => o.MapFrom(m => Guid.NewGuid()));

            CreateMap<AlterarLojaRequestDto, Modelo.Fidelidade.Data.Loja>()
                .ForMember(o => o.ChaveIntegracao, o => o.MapFrom(m => m.ChaveIntegracao))
                .ForMember(o => o.Codigo, o => o.MapFrom(m => m.Codigo))
                .ForMember(o => o.Id, o => o.Ignore());

            CreateMap<ClienteLoja, ConsultaFidelizacaoRequestDto>()
                .ForMember(o => o.ChaveIntegracao, o => o.MapFrom(m => m.Loja.ChaveIntegracao.ToString()))
                .ForMember(o => o.CodigoLoja, o => o.MapFrom(m => m.Loja.Codigo.ToString()))
                .ForMember(o => o.NumeroCartao, o => o.MapFrom(m => m.NumeroCartaoFidelidade))
                .ForMember(o => o.NsuCliente, o => o.MapFrom(m => Guid.NewGuid()));
        }
    }
}
