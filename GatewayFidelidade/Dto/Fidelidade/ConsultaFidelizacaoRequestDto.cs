﻿namespace GatewayFidelidade.Dto.Fidelidade
{
    public class ConsultaFidelizacaoRequestDto
    {
        public string ChaveIntegracao { get; set; }
        public string CodigoLoja { get; set; }
        public string NumeroCartao { get; set; }
        public string NsuCliente { get; set; }
    }
}
