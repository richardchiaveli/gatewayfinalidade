﻿using Modelo.Fidelidade.Data;
using System.Collections.Generic;

namespace GatewayFidelidade.Dto.Fidelidade
{
    public class PontosPorClienteResponseDto
    {
        public ConsultaFidelizacaoResponseDto Dados { get; set; }
        public List<HistoricoConsultaFidelidade> Historico { get; set; }
    }
}
