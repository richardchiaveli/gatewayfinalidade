﻿namespace GatewayFidelidade.Dto.Fidelidade
{
    public class PontosPorClienteRequestDto
    {
        public string CPF { get; set; }
        public string RG { get; set; }
    }
}
