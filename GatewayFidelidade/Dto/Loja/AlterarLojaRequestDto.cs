﻿using System;

namespace GatewayFidelidade.Dto.Loja
{
    public class AlterarLojaRequestDto
    {
        public Guid ChaveIntegracao { get; set; }
        public long Codigo { get; set; }
    }
}
