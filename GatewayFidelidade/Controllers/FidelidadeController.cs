﻿using AutoMapper;
using GatewayFidelidade.Controllers.Base;
using GatewayFidelidade.Dto.Fidelidade;
using GatewayFidelidade.Dto.Loja;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Modelo.Fidelidade.Data;
using Servico.Fidelidade.Interfaces;
using System;
using System.Linq;
using System.Net.Http;

namespace GatewayFidelidade.Controllers
{
    /// <summary>
    /// Controller Fidelidade
    /// </summary>
    [Route("gateway/fidelidade")]
    [ApiController]
    public class FidelidadeController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        private readonly IServicoClienteLoja _servicoClienteLoja;
        private readonly IServicoHistoricoConsultaFidelidade _servicoHistoricoConsultaFidelidade;
        private readonly IServicoLoja _servicoLoja;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="configuration"></param>
        /// <param name="servicoClienteLoja"></param>
        /// <param name="servicoHistoricoConsultaFidelidade"></param>
        /// <param name="servicoLoja"></param>
        public FidelidadeController(
            IMapper mapper,
            IConfiguration configuration,
            IServicoClienteLoja servicoClienteLoja,
            IServicoHistoricoConsultaFidelidade servicoHistoricoConsultaFidelidade,
            IServicoLoja servicoLoja)
        {
            _mapper = mapper;
            _configuration = configuration;
            _servicoClienteLoja = servicoClienteLoja;
            _servicoHistoricoConsultaFidelidade = servicoHistoricoConsultaFidelidade;
            _servicoLoja = servicoLoja;
        }

        /// <summary>
        /// Listar pontos e saldo do cliente
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        [HttpPost("pontos/cliente")]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(PontosPorClienteResponseDto), StatusCodes.Status200OK)]
        public ActionResult<PontosPorClienteResponseDto> PostListarPontosPorCliente(
            [FromBody] PontosPorClienteRequestDto body)
        {
            try
            {
                var clienteLoja = _servicoClienteLoja.BuscarPorDocumentoCliente(body.RG, body.CPF);
                if (clienteLoja == null)
                    return NotFound("Cliente não encontrado");

                var retorno = new PontosPorClienteResponseDto();

                var httpClient = ClientApi.GetHttpClient();
                {
                    var responseMessage = httpClient.PostAsync(
                        ClientApi.GetFidelidadeUri(_configuration, "ConsultaFidelizacao"),
                        ClientApi.GetFromBody(_mapper.Map<ConsultaFidelizacaoRequestDto>(clienteLoja))
                    ).Result;

                    if (!responseMessage.IsSuccessStatusCode && responseMessage.StatusCode 
                        == System.Net.HttpStatusCode.InternalServerError)
                    {
                        return StatusCode(StatusCodes.Status500InternalServerError, $"Fidelidade API: {responseMessage.StatusCode}: " +
                             $"{responseMessage.Content.ReadAsStringAsync().Result}");
                    }
                    else
                    {
                        retorno.Dados = responseMessage.Content.ReadAsAsync<ConsultaFidelizacaoResponseDto>().Result;

                        var historicoInserir = _mapper.Map<HistoricoConsultaFidelidade>(retorno.Dados);
                        _servicoHistoricoConsultaFidelidade.Adicionar(historicoInserir, out var listaHistorico);
                        retorno.Historico = listaHistorico.ToList();
                    }
                }

                return Ok(retorno);
            }
            catch (Exception ex)
            {
                return ApiHandleExceptions(ex);
            }
        }

        /// <summary>
        /// Alterar dados da loja
        /// </summary>
        /// <param name="id"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        [HttpPut("loja/{id}")]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(PontosPorClienteResponseDto), StatusCodes.Status200OK)]
        public ActionResult<PontosPorClienteResponseDto> PostPontosPorCliente(
            Guid id, [FromBody] AlterarLojaRequestDto body)
        {
            try
            {
                var obj = _mapper.Map<Loja>(body);
                obj.Id = id;

                return Ok(_servicoLoja.Atualizar(obj));
            }
            catch (Exception ex)
            {
                return ApiHandleExceptions(ex);
            }
        }
    }
}
