﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Repositorio.Fidelidade.Helper;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using static GatewayFidelidade.Startup;

namespace GatewayFidelidade.Controllers.Base
{
    public class BaseController : ControllerBase
    {
        [ApiExplorerSettings(IgnoreApi = true)]
        public ActionResult ApiHandleExceptions(Exception ex)
        {
            ActionResult actionResult;

            switch (ex)
            {
                case InvalidException _:
                case ArgumentException _:
                    actionResult = BadRequest(ex.Message);
                    break;
                case NotFoundException _:
                    actionResult = NotFound(ex.Message);
                    break;
                default:
                    actionResult = StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
                    break;
            }

            return actionResult;
        }
    }

    internal static class ClientApi
    {
        public static HttpClient GetHttpClient(string authorizationHeaderValue = null)
        {
            var hubHttpClient = new HttpClient
            {
                Timeout = TimeSpan.FromMinutes(5)
            };

            hubHttpClient.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (authorizationHeaderValue != null)
            {
                hubHttpClient.DefaultRequestHeaders
                    .Add("Authorization", authorizationHeaderValue);
            }

            return hubHttpClient;
        }

        public static Uri GetFidelidadeUri(IConfiguration configuration, string requestUri)
        {
            var url = new Api();
            configuration.Bind("Configuracao:API", url);

            string defaultUri = url.Fidelidade;

            Uri baseUri = new Uri(defaultUri);

            return new Uri(baseUri, requestUri);
        }

        public static StringContent GetFromBody(object obj) =>
            new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json");
    }
}