﻿using AutoMapper;
using GatewayFidelidade.Dto.AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Repositorio.Fidelidade.Config;
using Repositorio.Fidelidade.Contexto;
using Servico.Fidelidade;
using Servico.Fidelidade.Interfaces;
using Swashbuckle.AspNetCore.Swagger;
using System.IO;
using System.Net;
using System.Reflection;

namespace GatewayFidelidade
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        private readonly string API_VERSION = Assembly.GetEntryAssembly()
                .GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var config = new BancoDados();
            Configuration.Bind("Configuracao:BancoDados", config);

            services.AddScoped<DbContext, ContextoFidelidade>();
            services.AddSingleton<IConfiguracaoBancoDados, BancoDados>(serviceProvider =>
            {
                return config;
            });
            services.AddScoped<IServicoClienteLoja, ServicoClienteLoja>();
            services.AddScoped<IServicoHistoricoConsultaFidelidade, ServicoHistoricoConsultaFidelidade>();
            services.AddScoped<IServicoLoja, ServicoLoja>();

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperProfile());
            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddCors(
                options => options.AddPolicy("AllowCors",
                builder =>
                {
                    builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                })
            );

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(API_VERSION,
                    new Info
                    {
                        Title = "Gateway Fidelidade",
                        Version = API_VERSION,
                        Contact = new Contact
                        {
                            Name = "Richard Cleyton Chiaveli",
                            Email = "richard.chiaveli@outlook.com"
                        }
                    });

                c.DescribeAllEnumsAsStrings();

                string caminhoAplicacao = PlatformServices.Default.Application.ApplicationBasePath;
                string nomeAplicacao = PlatformServices.Default.Application.ApplicationName;
                string caminhoXmlDoc = Path.Combine(caminhoAplicacao, $"{nomeAplicacao}.xml");

                if (File.Exists(caminhoXmlDoc))
                {
                    c.IncludeXmlComments(caminhoXmlDoc);
                }
            });

            services.AddHttpContextAccessor();
            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("AllowCors"));
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseStaticFiles();
            app.UseCors("AllowCors");
            app.UseExceptionHandler(
              builder =>
              {
                  builder.Run(
                    async context =>
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        context.Response.Headers.Add("Access-Control-Allow-Origin", "*");

                        var error = context.Features.Get<IExceptionHandlerFeature>();
                        if (error != null)
                        {
                            context.Response.Headers.Add("Application-Error", error.Error.Message);
                            context.Response.Headers.Add("access-control-expose-headers", "Application-Error");
                            await context.Response.WriteAsync(error.Error.Message).ConfigureAwait(false);
                        }
                    });
              });

            app.UseHttpsRedirection();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

            });
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"/swagger/{API_VERSION}/swagger.json", "Gateway Fidelidade");
            });
        }

        internal class Appsettings
        {
            public Configuracao Configuracao { get; set; }
        }

        internal class BancoDados : IConfiguracaoBancoDados
        {
            public string EnderecoServidor { get; set; }
            public string NomeBaseDados { get; set; }
            public string Porta { get; set; }
            public string Esquema { get; set; }
            public string Usuario { get; set; }
            public string Senha { get; set; }
        }

        public class Api
        {
            public string Fidelidade { get; set; }
        }

        internal class Configuracao
        {
            public BancoDados BancoDados { get; set; }
            public Api API { get; set; }
        }
    }
}
