﻿using System;
using System.Collections.Generic;

namespace Modelo.Fidelidade.Data.Base
{
    public interface IModeloBase
    {
        Guid Id { get; set; }

        bool Validar(out List<string> erros);
    }
}
