﻿using Modelo.Fidelidade.Data.Base;
using Modelo.Fidelidade.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Modelo.Fidelidade.Data
{
    public class Cliente : IModeloBase
    {
        [Key]
        public Guid Id { get; set; }

        [ForeignKey("FK_Cliente_Contato")]
        public Guid IdContato { get; set; }
        public virtual Contato Contato { get; set; }

        [StringLength(100)]
        public string Nome { get; set; }
        [StringLength(11)]
        public string CPF { get; set; }
        [StringLength(9)]
        public string RG { get; set; }
        public Sexo Genero { get; set; }
        public DateTime DataNascimento { get; set; }

        public bool Validar(out List<string> erros)
        {
            erros = new List<string>();
            bool valido = true;

            if (Id == Guid.Empty)
            {
                erros.Add("Identificador do cliente inválido");
                valido = false;
            }

            if (IdContato == Guid.Empty)
            {
                erros.Add("Identificador do contato inválido");
                valido = false;
            }

            if (string.IsNullOrWhiteSpace(Nome))
            {
                erros.Add("Nome do cliente inválido");
                valido = false;
            }

            if (string.IsNullOrWhiteSpace(CPF) || CPF.Length != 11 || 
                !System.Text.RegularExpressions.Regex.IsMatch(CPF, "^[0-9]*$"))
            {
                erros.Add("CPF inválido");
                valido = false;
            }

            if (string.IsNullOrWhiteSpace(RG) || RG.Length != 9)
            {
                erros.Add("RG do cliente inválido");
                valido = false;
            }

            if (DataNascimento == DateTime.MinValue)
            {
                erros.Add("Data do nacimento do cliente inválido");
                valido = false;
            }

            return valido;
        }
    }
}
