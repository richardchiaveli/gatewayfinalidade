﻿using Modelo.Fidelidade.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Modelo.Fidelidade.Data
{
    public class Contato : IModeloBase
    {
        [Key]
        public Guid Id { get; set; }
        public long Telefone { get; set; }
        public long Celular { get; set; }
        [StringLength(100)]
        public string Email { get; set; }
        
        public bool Validar(out List<string> erros)
        {
            erros = new List<string>();
            bool valido = true;

            if (Id == Guid.Empty)
            {
                erros.Add("Identificador do contato inválido");
                valido = false;
            }

            if (Telefone <= 0 || Telefone.ToString().Length != 10 || 
                !System.Text.RegularExpressions.Regex.IsMatch(Telefone.ToString(), "^[0-9]*$"))
            {
                erros.Add("Telefone inválido");
                valido = false;
            }

            if (Celular <= 0 && Celular.ToString().Length != 11 || 
                !System.Text.RegularExpressions.Regex.IsMatch(Celular.ToString(), "^[0-9]*$"))
            {
                erros.Add("Celular inválido");
                valido = false;
            }

            if (string.IsNullOrWhiteSpace(Email) || 
                !Email.Contains("@") || !Email.ToUpper().Contains(".COM"))
            {
                erros.Add("E-mail inválido");
                valido = false;
            }

            return valido;
        }
    }
}
