﻿namespace Modelo.Fidelidade.Data.Enums
{
    public enum Sexo
    {
        Masculino = 1,
        Feminino = 2
    }
}
