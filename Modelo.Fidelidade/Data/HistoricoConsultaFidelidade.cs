﻿using Modelo.Fidelidade.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Modelo.Fidelidade.Data
{
    public class HistoricoConsultaFidelidade : IModeloBase
    {
        [Key]
        public Guid Id { get; set; }
        public Guid NsuCliente { get; set; }
        public int QuantidadePontos { get; set; }
        public decimal PontosEmReais { get; set; }

        public bool Validar(out List<string> erros)
        {
            erros = new List<string>();
            bool valido = true;

            if (Id == Guid.Empty)
            {
                erros.Add("Identificador do cliente inválido");
                valido = false;
            }

            if (NsuCliente == Guid.Empty)
            {
                erros.Add("NSU do cliente inválido");
                valido = false;
            }

            return valido;
        }
    }
}
