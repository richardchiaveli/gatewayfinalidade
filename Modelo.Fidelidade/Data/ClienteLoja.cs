﻿using Modelo.Fidelidade.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Modelo.Fidelidade.Data
{
    public class ClienteLoja : IModeloBase
    {
        [Key]
        public Guid Id { get; set; }

        [ForeignKey("FK_ClienteLoja_Cliente")]
        public Guid IdCliente { get; set; }
        public virtual Cliente Cliente { get; set; }

        [ForeignKey("FK_ClienteLoja_Loja")]
        public Guid IdLoja { get; set; }
        public virtual Loja Loja { get; set; }

        public string NumeroCartaoFidelidade { get; set; }

        public bool Validar(out List<string> erros)
        {
            erros = new List<string>();
            bool valido = true;

            if (Id == Guid.Empty)
            {
                erros.Add("Identificador inválido");
                valido = false;
            }

            if (IdCliente == Guid.Empty)
            {
                erros.Add("Identificador do cliente inválido");
                valido = false;
            }

            if (IdLoja == Guid.Empty)
            {
                erros.Add("Identificador da loja inválido");
                valido = false;
            }

            if (string.IsNullOrWhiteSpace(NumeroCartaoFidelidade))
            {
                erros.Add("Número do cartão inválido");
                valido = false;
            }

            return valido;
        }
    }
}
