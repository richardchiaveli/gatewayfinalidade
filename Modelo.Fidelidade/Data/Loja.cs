﻿using Modelo.Fidelidade.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Modelo.Fidelidade.Data
{
    public class Loja : IModeloBase
    {
        [Key]
        public Guid Id { get; set; }
        public Guid ChaveIntegracao { get; set; }
        public long Codigo { get; set; }

        public bool Validar(out List<string> erros)
        {
            erros = new List<string>();
            bool valido = true;

            if (Id == Guid.Empty)
            {
                erros.Add("Identificador da loja inválido");
                valido = false;
            }

            if (ChaveIntegracao == Guid.Empty)
            {
                erros.Add("Chave de integração inválido");
                valido = false;
            }

            if (Codigo <= 0)
            {
                erros.Add("Código inválido");
                valido = false;
            }

            return valido;
        }
    }
}
