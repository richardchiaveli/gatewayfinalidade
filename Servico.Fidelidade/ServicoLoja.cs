﻿using Microsoft.EntityFrameworkCore;
using Modelo.Fidelidade.Data;
using Repositorio.Fidelidade;
using Repositorio.Fidelidade.Helper;
using Servico.Fidelidade.Interfaces;

namespace Servico.Fidelidade
{
    public class ServicoLoja : RepositorioBase<Loja>, IServicoLoja
    {
        public ServicoLoja(DbContext contexto) : base(contexto)
        {
        }

        public bool Atualizar(Loja entidade)
        {
            if (!Verificar(s => s.Id == entidade.Id))
                throw new NotFoundException("Loja não encontrada.");

            var dados = ObterPorId(entidade.Id);

            if (entidade.ChaveIntegracao != dados.ChaveIntegracao)
                dados.ChaveIntegracao = entidade.ChaveIntegracao;

            if (entidade.Codigo != dados.Codigo)
                dados.Codigo = entidade.Codigo;

            return Atualizar(entidades: dados);
        }
    }
}
