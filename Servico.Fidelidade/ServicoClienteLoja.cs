﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using Modelo.Fidelidade.Data;
using Repositorio.Fidelidade;
using Servico.Fidelidade.Interfaces;
using System;

namespace Servico.Fidelidade
{
    public class ServicoClienteLoja : RepositorioBase<ClienteLoja>, IServicoClienteLoja
    {
        public ServicoClienteLoja(DbContext contexto) : base(contexto)
        {
        }

        public ClienteLoja BuscarPorDocumentoCliente(string RG = null, string CPF = null)
        {
            if (string.IsNullOrWhiteSpace(RG) && (string.IsNullOrWhiteSpace(CPF) || CPF.Length > 11))
                throw new ArgumentException("RG ou CPF devem ser informados.");

            var filtros = PredicateBuilder.New<ClienteLoja>();

            if (!string.IsNullOrWhiteSpace(RG))
                filtros.And(p => p.Cliente.RG == RG.Trim());

            if (!string.IsNullOrWhiteSpace(CPF) && CPF.Length <= 11)
                filtros.And(p => p.Cliente.CPF == CPF);

            return ObterPorFiltros(filtros, i => i.Cliente, i => i.Loja);
        }
    }
}
