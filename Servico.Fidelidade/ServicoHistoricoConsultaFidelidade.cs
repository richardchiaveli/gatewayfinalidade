﻿using Microsoft.EntityFrameworkCore;
using Modelo.Fidelidade.Data;
using Repositorio.Fidelidade;
using Servico.Fidelidade.Interfaces;
using System.Collections.Generic;

namespace Servico.Fidelidade
{
    public class ServicoHistoricoConsultaFidelidade : RepositorioBase<HistoricoConsultaFidelidade>, 
        IServicoHistoricoConsultaFidelidade
    {
        public ServicoHistoricoConsultaFidelidade(DbContext contexto) : base(contexto)
        {
        }

        public bool Adicionar(HistoricoConsultaFidelidade entidade, 
            out IEnumerable<HistoricoConsultaFidelidade> retorno)
        {
            var sucesso = Adicionar(entidades: entidade);
            retorno = BuscarTudo();

            return sucesso;
        }
    }
}
