﻿using Modelo.Fidelidade.Data;

namespace Servico.Fidelidade.Interfaces
{
    public interface IServicoLoja
    {
        bool Atualizar(Loja entidade);
    }
}
