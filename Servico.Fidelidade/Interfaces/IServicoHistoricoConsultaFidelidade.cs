﻿using Modelo.Fidelidade.Data;
using System.Collections.Generic;

namespace Servico.Fidelidade.Interfaces
{
    public interface IServicoHistoricoConsultaFidelidade
    {
        bool Adicionar(HistoricoConsultaFidelidade entidade,
            out IEnumerable<HistoricoConsultaFidelidade> retorno);
    }
}
