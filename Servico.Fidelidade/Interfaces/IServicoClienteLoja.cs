﻿using Modelo.Fidelidade.Data;

namespace Servico.Fidelidade.Interfaces
{
    public interface IServicoClienteLoja
    {
        ClienteLoja BuscarPorDocumentoCliente(string RG = null, string CPF = null);
    }
}
