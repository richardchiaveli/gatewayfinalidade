# Gateway Fidelidade
### Informações da aplicação:
- Dotnet Core Api com Entity Framework
- Microsoft SQL Server
### Dica !!!
  - Mudar as configurações de banco no appsettings.json. Segue abaixo exemplo de configuração:
```json    
  "BancoDados": {
     "EnderecoServidor": "DESKTOP-AHP4U75",
     "NomeBaseDados": "Fidelidade",
     "Usuario": "sa",
     "Senha": "123"
  }
```
 - Foi configurado no Entity Framework para o banco ser inicializado com alguns dados. Segue dados abaixo:
- ##### OnDataBaseInitialize (REPOSITÓRIO)

> ##### Cliente
> - Id => "94366fa4-76ec-4371-8807-06946957b299"
> - CPF => "40612907899"
> - RG => "171849462"

> ##### Loja
> - Id => "eff5f00d-9232-42ff-8fb5-2615c2aa6ead"
> - ChaveIntegracao => "4B335B6F-9C4D-47F7-B798-C46FFBC4881A"
> - Codigo => 1
