﻿using Microsoft.EntityFrameworkCore;
using Modelo.Fidelidade.Data.Base;
using Repositorio.Fidelidade.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Repositorio.Fidelidade
{
    public class RepositorioBase<TEntity> where TEntity : class, IModeloBase, new()
    {
        private readonly DbSet<TEntity> _dbSet;
        private readonly DbContext _contexto;

        protected internal RepositorioBase(DbContext contexto)
        {
            _contexto = contexto;
            _dbSet = _contexto.Set<TEntity>();
        }

        public bool Adicionar(params TEntity[] entidades)
        {
            foreach (var entidade in entidades)
            {
                if (entidade.Validar(out var erros))
                    _dbSet.Add(entidade).State = EntityState.Added;
                else
                    throw new InvalidException($"Entidade inválida. " +
                        $"Erros encontrados: {string.Join(", ", erros)}");
            }
            
            return Salvar();
        }

        public bool Atualizar(params TEntity[] entidades)
        {
            foreach (var entidade in entidades)
            {
                if (entidade.Validar(out var erros))
                    _dbSet.Update(entidade).State = EntityState.Modified;
                else
                    throw new InvalidException($"Entidade inválida. " +
                        $"Erros encontrados: {string.Join(", ", erros)}");
            }

            return Salvar();
        }

        public bool Deletar(params TEntity[] entidades)
        {
            foreach (var entidade in entidades)
            {
                _dbSet.Remove(entidade).State = EntityState.Deleted;
            }

            return Salvar();
        }

        public bool DeletarPorFiltros(Expression<Func<TEntity, bool>> filtros)
        {
            IEnumerable<TEntity> entities = _dbSet.Where(filtros);

            foreach (var entity in entities)
            {
                _dbSet.Remove(entity).State = EntityState.Deleted;
            }

            return Salvar();
        }

        public TEntity ObterPorId(Guid id, params Expression<Func<TEntity, object>>[] tabelasRelacionadas)
        {
            IQueryable<TEntity> query = _contexto.Set<TEntity>();

            if (tabelasRelacionadas.IncludeHasProperties())
            {
                tabelasRelacionadas.ToList().ForEach(property =>
                {
                    _dbSet.Include(property);
                });
            }

            return query.FirstOrDefault(x => x.Id == id);
        }

        public TEntity ObterPorFiltros(Expression<Func<TEntity, bool>> filtros,
            params Expression<Func<TEntity, object>>[] tabelasRelacionadas)
        {
            IQueryable<TEntity> query = _dbSet;

            if (tabelasRelacionadas.IncludeHasProperties())
            {
                tabelasRelacionadas.ToList().ForEach(property =>
                {
                    query = query.Include(property);
                });
            }

            return query.FirstOrDefault(filtros);
        }

        public IEnumerable<TEntity> BuscarTudo(params Expression<Func<TEntity, object>>[] tabelasRelacionadas)
        {
            IQueryable<TEntity> query = _dbSet;

            if (tabelasRelacionadas.IncludeHasProperties())
            {
                tabelasRelacionadas.ToList().ForEach(property =>
                {
                    query = query.Include(property);
                });
            }

            return query;
        }

        public IEnumerable<TEntity> BuscarTudoPaginado(int indice, int tamanhoPagina, out int totalPaginas,
            out int totalRegistros, string ordenarPor = "Id", SortOrder tipoOrdenacao = SortOrder.Ascending,
            params Expression<Func<TEntity, object>>[] tabelasRelacionadas)
        {
            IQueryable<TEntity> query = _dbSet;

            if (tabelasRelacionadas.IncludeHasProperties())
            {
                tabelasRelacionadas.ToList().ForEach(property =>
                {
                    query = query.Include(property);
                });
            }

            return ((IEnumerable<TEntity>)query).
                PagedIndex(indice, tamanhoPagina, out totalPaginas, out totalRegistros, ordenarPor, tipoOrdenacao);
        }

        public IEnumerable<TEntity> BuscarPorFiltros(Expression<Func<TEntity, bool>> filtros,
            params Expression<Func<TEntity, object>>[] tabelasRelacionadas)
        {
            IQueryable<TEntity> query = _dbSet;

            if (tabelasRelacionadas.IncludeHasProperties())
            {
                tabelasRelacionadas.ToList().ForEach(property =>
                {
                    query = query.Include(property);
                });
            }

            return query.Where(filtros);
        }

        public IEnumerable<TEntity> BuscarPorFiltrosPaginado(int indice, int tamanhoPagina, out int totalPaginas,
            out int totalRegistros, Expression<Func<TEntity, bool>> filtros, string ordenarPor = "Id",
            SortOrder tipoOrdenacao = SortOrder.Ascending, params Expression<Func<TEntity, object>>[] tabelasRelacionadas)
        {
            IQueryable<TEntity> query = _dbSet?.Where(filtros);

            if (tabelasRelacionadas.IncludeHasProperties())
            {
                tabelasRelacionadas.ToList().ForEach(property =>
                {
                    query = query.Include(property);
                });
            }

            return ((IEnumerable<TEntity>)query).
                PagedIndex(indice, tamanhoPagina, out totalPaginas, out totalRegistros, ordenarPor, tipoOrdenacao);
        }

        public bool Verificar(Expression<Func<TEntity, bool>> filtros) =>
            filtros.PredicateHasFilter() ? _dbSet.Any(filtros) : _dbSet.Any();

        public int Contar(Expression<Func<TEntity, bool>> filtros) =>
            filtros.PredicateHasFilter() ? _dbSet.Count(filtros) : _dbSet.Count();

        private bool Salvar() => _contexto != null && _contexto.SaveChanges() > 0;
    }
}
