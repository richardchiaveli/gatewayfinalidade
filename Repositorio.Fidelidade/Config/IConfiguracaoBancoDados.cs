﻿namespace Repositorio.Fidelidade.Config
{
    public interface IConfiguracaoBancoDados
    {
        string EnderecoServidor { get; set; }
        string Porta { get; set; }
        string NomeBaseDados { get; set; }
        string Esquema { get; set; }
        string Usuario { get; set; }
        string Senha { get; set; }
    }
}
