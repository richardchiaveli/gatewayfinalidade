﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Modelo.Fidelidade.Data;
using Repositorio.Fidelidade.Config;
using System;

namespace Repositorio.Fidelidade.Contexto
{
    public class ContextoFidelidade : DbContext
    {
        private readonly IConfiguracaoBancoDados _configuracaoBancoDados;

        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<ClienteLoja> ClienteLoja { get; set; }
        public DbSet<Contato> Contato { get; set; }
        public DbSet<HistoricoConsultaFidelidade> HistoricoConsultaFidelidade { get; set; }
        public DbSet<Loja> Loja { get; set; }

        public string ConnectionString
        {
            get
            {
                return $"User ID={_configuracaoBancoDados.Usuario}; " +
                       $"Database={_configuracaoBancoDados.NomeBaseDados};" +
                       $"Server={_configuracaoBancoDados.EnderecoServidor}; " +
                       $"Password={_configuracaoBancoDados.Senha};";
            }
        }

        public ContextoFidelidade(IConfiguracaoBancoDados configuracaoBancoDados)
        {
            _configuracaoBancoDados = configuracaoBancoDados;
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                //.UseLazyLoadingProxies()
                .UseSqlServer(ConnectionString);

            optionsBuilder.UseLoggerFactory(new LoggerFactory().AddConsole((category, level) =>
             level == LogLevel.Information &&
                category == DbLoggerCategory.Database.Command.Name, true));
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            OnDataBaseTableDefinition(modelBuilder);
            OnDataBaseTypeConfiguration(modelBuilder, _configuracaoBancoDados.Esquema);
            OnDataBaseInitialize(modelBuilder);
            base.OnModelCreating(modelBuilder);
        }

        private void OnDataBaseTypeConfiguration(ModelBuilder modelBuilder, string schema)
        {
            if (!string.IsNullOrWhiteSpace(schema))
            {
                modelBuilder.HasDefaultSchema(schema);
            }
        }

        private void OnDataBaseTableDefinition(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cliente>(cliente =>
            {
                cliente.ToTable(typeof(Cliente).Name);
                cliente.HasOne(c => c.Contato).WithMany();
            });

            modelBuilder.Entity<ClienteLoja>(clienteLoja =>
            {
                clienteLoja.ToTable(typeof(ClienteLoja).Name);
                clienteLoja.HasOne(c => c.Cliente).WithOne().HasForeignKey<ClienteLoja>(c => c.IdCliente);
                clienteLoja.HasOne(c => c.Loja).WithOne().HasForeignKey<ClienteLoja>(c => c.IdLoja);
            });

            modelBuilder.Entity<Contato>().ToTable(typeof(Contato).Name);

            modelBuilder.Entity<HistoricoConsultaFidelidade>().ToTable(typeof(HistoricoConsultaFidelidade).Name);

            modelBuilder.Entity<Loja>().ToTable(typeof(Loja).Name);
        }

        private void OnDataBaseInitialize(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cliente>().HasData(
                new Cliente
                {
                    Id = new Guid("94366fa4-76ec-4371-8807-06946957b299"),
                    IdContato = new Guid("77ede43b-c495-4004-ad67-95f470946ac3"),
                    Nome = "RICHARD CLEYTON CHIAVELI",
                    Genero = Modelo.Fidelidade.Data.Enums.Sexo.Masculino,
                    DataNascimento = new DateTime(1995, 10, 29),
                    CPF = "40612907899",
                    RG = "171849462"
                }
            );

            modelBuilder.Entity<Contato>().HasData(
               new Contato
               {
                   Id = new Guid("77ede43b-c495-4004-ad67-95f470946ac3"),
                   Email = "RICHARD.CHIAVELI@OUTLOOK.COM",
                   Telefone = 1436968663,
                   Celular = 14999331804
               }
           );

            modelBuilder.Entity<Loja>().HasData(
                new Loja
                {
                    Id = new Guid("eff5f00d-9232-42ff-8fb5-2615c2aa6ead"),
                    ChaveIntegracao = new Guid("4B335B6F-9C4D-47F7-B798-C46FFBC4881A"),
                    Codigo = 1
                }
            );

            modelBuilder.Entity<ClienteLoja>().HasData(
                new ClienteLoja
                {
                    Id = new Guid("c0371a1c-c08b-48d9-b9c2-8c5f8e313c8d"),
                    IdCliente = new Guid("94366fa4-76ec-4371-8807-06946957b299"),
                    IdLoja = new Guid("eff5f00d-9232-42ff-8fb5-2615c2aa6ead"),
                    NumeroCartaoFidelidade = "32231126850"
                }
            );
        }
    }
}
