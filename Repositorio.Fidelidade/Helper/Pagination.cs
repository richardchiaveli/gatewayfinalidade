﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Repositorio.Fidelidade.Helper
{
    public enum SortOrder : short
    {
        Ascending = 1,
        Descending = 2
    }

    public static class Pagination
    {
        public static IEnumerable<T> PagedIndex<T>(this IEnumerable<T> query, int pageIndex, int pageSize,
            out int totalPages, out int totalItems, string orderBy = "Id", SortOrder sort = SortOrder.Ascending)
        {
            totalItems = query.Count();
            totalPages = GetTotalPages(totalItems, pageSize);

            query = sort == SortOrder.Ascending ?
                query.OrderBy(s => s.TryGetProperty(orderBy, true)) :
                query.OrderByDescending(s => s.TryGetProperty(orderBy, true));

            return query.Skip(GetSkip(pageIndex, pageSize)).Take(pageSize);
        }

        private static int GetSkip(int pageIndex, int pageSize) => (pageIndex - 1) * pageSize;

        private static int GetTotalPages(int totalItems, int itemsPerPage)
        {
            var totalPages = totalItems / itemsPerPage;

            if (totalItems % itemsPerPage != 0)
            {
                totalPages++;
            }

            return totalPages;
        }

        private static PropertyInfo TryGetProperty(this object obj, string property,
            bool checkReading = false, bool checkWriting = false)
        {
            var prop = obj.GetType().GetProperty(property, BindingFlags.Public | BindingFlags.Instance);

            return (prop is null || (checkReading && !prop.CanRead) || (checkWriting && !prop.CanWrite)) ? null : prop;
        }
    }
}
