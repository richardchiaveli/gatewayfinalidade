﻿using System;
using System.Linq.Expressions;

namespace Repositorio.Fidelidade.Helper
{
    public static class ExpressionValidator
    {
        public static bool PredicateHasFilter<T>(this Expression<Func<T, bool>> predicate) =>
            predicate != null && !predicate.Parameters[0].Name.Equals("f") &&
                !predicate.ToString().Equals("f => False");

        public static bool IncludeHasProperties<T>(this Expression<Func<T, object>>[] includeProperties) =>
            includeProperties != null && includeProperties.Length > 0;
    }
}
